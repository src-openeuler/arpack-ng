Name:		arpack
Version:	3.9.1
Release:	1
Summary:	Fortran 77 subroutines for solving large scale eigenvalue problems
License:	BSD
URL:		https://github.com/opencollab/arpack-ng
Source0:	https://github.com/opencollab/arpack-ng/archive/%{version}/arpack-ng-%{version}.tar.gz

BuildRequires:	gcc-gfortran openblas-devel libtool >= 2.4.2
Provides:	arpack-ng = %{version}-%{release}

%description
Calculating eigenvalues and eigenvectors of general n-times-n matrix A.

%package devel
Summary:	Files needed for developing arpack based applications
Requires:	arpack = %{version}-%{release}
Provides:	arpack-ng-devel = %{version}-%{release}

%description devel
This package is used to build the so library link of arpack's Application.

%package help
Summary:	Examples for the use of arpack
BuildArch:	noarch
Provides:       %{name}-doc = %{version}-%{release}
Obsoletes:      %{name}-doc < %{version}-%{release}

%description help
This package contains examples for the use of arpack.

%package static
Summary:	Static library for developing arpack based applications
Requires:	arpack-devel = %{version}-%{release}
Provides:	arpack-ng-static = %{version}-%{release}

%description static
Used to solve large-scale feature problems, including write library files.

%prep
%autosetup -c
mv arpack-ng-%{version} src
pushd src
autoreconf -vif
popd
cp -pr src src64

%build
%global blaslib -lopenblasp
pushd src
%configure --enable-shared --enable-static \
    --with-blas="%{blaslib}" \
    --with-lapack="%{blaslib}"
%make_build
popd
pushd src64
%configure --enable-shared --enable-static \
    LIBSUFFIX=64_ \
    SYMBOLSUFFIX=_64 \
    INTERFACE64=1 \
    --with-blas=%{blaslib}64_ \
    --with-lapack=%{blaslib}64_
%make_build
popd

%install
pushd src
%make_install
popd
pushd src64
%make_install
popd
rm -r %{buildroot}%{_libdir}/*.la

%check
pushd src
%make_build check
pushd EXAMPLES ; make clean ; popd
popd
pushd src64
%make_build check
pushd EXAMPLES ; make clean ; popd
popd

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%license src/COPYING
%{_libdir}/libarpack.so.*
%{_libdir}/libarpack64_.so.*

%files devel
%{_libdir}/pkgconfig/arpack.pc
%{_libdir}/libarpack.so
%{_libdir}/pkgconfig/arpack64_.pc
%{_libdir}/libarpack64_.so
%{_includedir}/arpack/
%{_libdir}/pkgconfig/parpack.pc
%{_libdir}/pkgconfig/parpack64_.pc


%files help
%doc src/EXAMPLES/ src/DOCUMENTS/ src/CHANGES src/README.md

%files static
%{_libdir}/libarpack.a
%{_libdir}/libarpack64_.a

%changelog
* Mon Jul 22 2024 xuguangmin <xuguangmin@kylinos.cn> -3.9.1-1
- update to 3.9.1

* Wed Aug 04 2021 chenchen <chen_aka_jan@163.com> -3.8.0-2
- del buildrequire atlas

* Tue Aug 03 2021 sunguoshuai <sunguoshuai@@huawei.com> -3.8.0-1
- Upgrade to 3.8.0

* Sat Mar 07 2020 wangye <wangye54@huawei.com> - 3.5.0-7
- Package init
